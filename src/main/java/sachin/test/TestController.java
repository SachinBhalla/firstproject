package sachin.test;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/testfor")
public class TestController 
{
	@GetMapping(value="/hello")
	public String Hello()
	{
		return "Hello From Spring Boot";
	}

}
