package com.example.Controller;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Entity.Student;
import com.example.Service.StudentServiceImplementation;

@RestController
@RequestMapping("/student")
public class StudentController 
{
	@Autowired
	StudentServiceImplementation studentservice;
	
	
	
	
	
	@GetMapping(value = "/findall")
	public List<Student> FindAllStudent(){
		return studentservice.getallstudent();
	}
	
	@PostMapping(value = "/save")
	public Student SaveStudent(@Valid @RequestBody Student student) {
		return studentservice.SaveStudent(student);
	}
	@GetMapping(value = "/getone/{id}")
	public Student getone(@PathVariable("id") Integer id)
	{
		return studentservice.GetOneStudent(id);
	}
	@DeleteMapping(value = "/delete/{id}")
	public void Delete(@PathVariable("id") Integer id) {
		studentservice.delete(id);
	}
	@GetMapping(value="/test")
	public String Hello()
	{
		return "Hello From Spring Boot";
	}
	

}
