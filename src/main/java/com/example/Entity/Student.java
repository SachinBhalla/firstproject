package com.example.Entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.Validation.ValidEmailServer;
import com.example.Validation.ValidPhoneServer;

@Entity
@Table(name = "student_record")
public class Student 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
    @NotNull(message = "is required")
	@Column(name="rollno")
	private Integer rollno;

	@NotNull(message = "is required")
	@Size(min = 3, max = 15,message="Please Enter a Length Between 3 to 15")
	@Column(name="sclass")
	private String sclass;

	@NotNull(message = "is required")
	@Size(min = 1, max = 15,message="Please Enter a Length Between 1 to 15")
	@Column(name="first_name")
	private String first_name;

	@NotNull(message = "is required")
	@Size(min = 1, max = 15,message="Please Enter a Length Between 1 to 15")
	@Column(name="last_name")
	private String last_name;
   
	@ValidEmailServer
	@NotNull(message = "is required")
	@Size(min = 3, message="is reqired")
	@Column(name="email")
	private String email;
    
    @ValidPhoneServer
    @Size(min=10 ,message="is required")
    @Column(name="phone_no")
    private String phoneno;
    
	
	
	@Autowired
	public Student(Integer id, @NotNull(message = "is required") Integer rollno,
			@NotNull(message = "is required") @Size(min = 3, max = 15, message = "Please Enter a Length Between 3 to 15") String sclass,
			@NotNull(message = "is required") @Size(min = 1, max = 15, message = "Please Enter a Length Between 1 to 15") String first_name,
			@NotNull(message = "is required") @Size(min = 1, max = 15, message = "Please Enter a Length Between 1 to 15") String last_name,
			@NotNull(message = "is required") @Size(min = 3, message = "is reqired") String email,
			@Size(min=10,message = "is required") String phoneno) {
		super();
		this.id = id;
		this.rollno = rollno;
		this.sclass = sclass;
		this.first_name = first_name;
		this.last_name = last_name;
		this.email = email;
		this.phoneno = phoneno;
	}

	public Student() 
	{
		super();
		System.out.println("Inside A Default Constructor");
		
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getRollno() {
		return rollno;
	}
	public void setRollno(Integer rollno) {
		this.rollno = rollno;
	}
	public String getSclass() {
		return sclass;
	}
	public void setSclass(String sclass) {
		this.sclass = sclass;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	

}
