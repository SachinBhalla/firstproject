package com.example.StudentDao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.Entity.Student;



@Repository
public interface IStudentDao  extends JpaRepository<Student, Integer>
{
	

}
