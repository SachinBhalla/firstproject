package com.example.QualfierAnnotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Testing")
public class ControllerMy 
{
	@Qualifier("cat")
	@Autowired
	Animal animal;
	
	@Qualifier("dog")
	@Autowired
	Animal animal2;
	
//	@PostConstruct
//	public void PostConstructExample()
//	{
//		System.out.println("Inside a post Construct Method");
//		
//	}
	
    @GetMapping(value="/hello")
    public String Hello()
    {
//    	String s=animal2.Charistics();
//    	System.out.println(s);
    	return animal.Charistics();
    	
    }
    
}
