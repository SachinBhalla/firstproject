package com.example.Service;
import java.util.List;
import com.example.Entity.Student;
public interface IStudentService 
{
	Student SaveStudent(Student student);
	List<Student> getallstudent();
	Student GetOneStudent(Integer id);
	void delete(Integer id);
}
