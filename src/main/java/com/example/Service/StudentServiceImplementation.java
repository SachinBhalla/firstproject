package com.example.Service;




import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.Entity.Student;
import com.example.StudentDao.IStudentDao;



@Service
public class StudentServiceImplementation  implements IStudentService
{
	@Autowired
	IStudentDao studentdao;
	

	@Override
	public Student SaveStudent(Student student) {
		return studentdao.save(student);
	}

	@Override
	public List<Student> getallstudent() {
	  List<Student>students=studentdao.findAll();
		return students ;
	}

	@Override
	public Student GetOneStudent(Integer id) {
		Student onestudent=studentdao.findById(id).orElse(new Student());
		return onestudent;
	}

	@Override
	public void delete(Integer id) {
		studentdao.deleteById(id);
		
	}
	

}
